import speech_recognition as sr
def main():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        r.adjust_for_ambient_noise(source)

        print("Lets Speak")

        audio = r.listen(source)

        print("Recognizing your voice .... ")


        try:
            print("Your text:\n" + r.recognize_google(audio))
            


        except Exception as e:
            print("No voice recognized!:  " + str(e))

        with open("recorded.wav", "wb") as f:
            f.write(audio.get_wav_data())


if __name__ == "__main__":
    main()
