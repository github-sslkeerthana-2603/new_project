import 'package:flutter/material.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Grammar App'),
    );
  }
}

class MyHomePage extends StatefulWidget {

  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text(widget.title),

      ),
      body: Center(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF0D47A1),
                    Color(0xFF1976D2),
                    Color(0xFF42A5F5),
                  ],
                ),
              ),
              width: MediaQuery.of(context).size.width,
              child: Row(

                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  new Wrap(
                    direction: Axis.horizontal,
                    crossAxisAlignment: WrapCrossAlignment.start,
                    spacing: 0,
                    runSpacing: 5,
                    children: [
                      RaisedButton(
                        onPressed: () {},
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color(0xFF0D47A1),
                                Color(0xFF1976D2),
                                Color(0xFF42A5F5),
                              ],
                            ),
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child:
                          const Text('Grammar', style: TextStyle(fontSize: 20)),
                        ),
                      ),
                      RaisedButton(
                        onPressed: () {},
                        textColor: Colors.white,
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color(0xFF0D47A1),
                                Color(0xFF1976D2),
                                Color(0xFF42A5F5),
                              ],
                            ),
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child:
                          const Text('Synonyms', style: TextStyle(fontSize: 20)),
                        ),
                      ),

                    ],
                  ),
                  new Wrap(
                    direction: Axis.horizontal,
                    crossAxisAlignment: WrapCrossAlignment.end,

                    spacing: 1,
                    runSpacing: 1,
                    children: [
                      IconButton(
                        onPressed: () {},
                        color: Colors.white,
                        icon: Icon(Icons.mic),
                        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      ),
                      IconButton(
                        onPressed: () {},
                        color: Colors.white,
                        icon: Icon(Icons.add_a_photo),
                        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      ),

                      IconButton(
                        onPressed: () {},
                        color: Colors.white,
                        icon: Icon(Icons.volume_up),
                        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      ),
                    ],
                  ),






                ],
              ),
            ),

            Container(

              width:MediaQuery.of(context).size.width,
              height: ((MediaQuery.of(context).size.height - 100)/2) ,
              color: Colors.grey[200],
              child: TextField(
                maxLines: 300,

                decoration: InputDecoration(

                    hintText: 'Enter text'
                ),
              ),
            ),
            Expanded(child: Container(
              width:MediaQuery.of(context).size.width,
              child: Center(
                child:SingleChildScrollView(
                  child:Column(

                    children:[ Container(

                      width: MediaQuery.of(context).size.width - 20,
                      //height: 100,
                      padding: new EdgeInsets.all(15.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        color: Colors.grey,
                        elevation: 10,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            const ListTile(
                              leading: Icon(Icons.warning, size: 60),
                              title: Text(
                                  'Example 1',
                                  style: TextStyle(fontSize: 30.0)
                              ),

                            ),


                            Wrap(
                              runSpacing: 5.0,
                              spacing: 5.0,
                              children: <Widget>[


                                RaisedButton(
                                  child: const Text('word1'),
                                  onPressed: () {},
                                ),
                                RaisedButton(
                                  child: const Text('word2'),
                                  onPressed: () {},
                                ),
                                RaisedButton(
                                  child: const Text('word3'),
                                  onPressed: () {},
                                ),RaisedButton(
                                  child: const Text('word4'),
                                  onPressed: () {},
                                ),RaisedButton(
                                  child: const Text('word5'),
                                  onPressed: () {},
                                ),

                              ],
                            ),

                          ],

                        ),
                      ),
                    ),
                      Container(

                        width: MediaQuery.of(context).size.width - 20,

                        padding: new EdgeInsets.all(10.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          color: Colors.grey,
                          elevation: 10,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              const ListTile(
                                leading: Icon(Icons.warning, size: 60),
                                title: Text(
                                    'Example 2',
                                    style: TextStyle(fontSize: 30.0)
                                ),

                              ),


                              Wrap(
                                runSpacing: 5.0,
                                spacing: 5.0,
                                children: <Widget>[


                                  RaisedButton(
                                    child: const Text('word1'),
                                    onPressed: () {},
                                  ),
                                  RaisedButton(
                                    child: const Text('word2'),
                                    onPressed: () {},
                                  ),
                                  RaisedButton(
                                    child: const Text('word3'),
                                    onPressed: () {},
                                  ),RaisedButton(
                                    child: const Text('word4'),
                                    onPressed: () {},
                                  ),RaisedButton(
                                    child: const Text('word5'),
                                    onPressed: () {},
                                  ),

                                ],
                              ),

                            ],

                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              color: Colors.grey[100],

            ),
            ),



          ],
        ),
      ),

      bottomNavigationBar: BottomNavigationBar(
        currentIndex: index,
        onTap: (int index) { setState((){ this.index = index; }); },
        items: <BottomNavigationBarItem> [
          new BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text("Home"),
          ),

          new BottomNavigationBarItem(
            icon: new Icon(Icons.translate),
            title: new Text("Translate"),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.menu_book),
            title: new Text("Dictionary"),
          ),

        ],

      ),
    );
  }
}
